import { useCallback, useEffect, useState } from "react";
import Modal from "react-modal";
import { toast } from "react-toastify";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { categorySchema } from "../schema/category";
import { request } from "../server/request";

const CategoriesP = () => {
  const [categories, setCategories] = useState([]);
  const [isOpen, setIsOpen] = useState(false);
  const [selected, setSelected] = useState(null);
  const [search, setSearch] = useState("");

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({ resolver: yupResolver(categorySchema) });

  const getData = useCallback(async () => {
    try {
      // let { data } = await axios.get(
      //   "https://640b314281d8a32198dce736.mockapi.io/api/v1/category"
      // );
      let { data } = await request("category", { params: { name: search } });
      setCategories(data);
    } catch (err) {
      toast.error(err.response.data);
    }
  }, [search]);

  useEffect(() => {
    getData();
  }, [getData]);

  function openModal() {
    setIsOpen(true);
  }

  function closeModal() {
    setIsOpen(false);
  }

  const onSubmit = async (data) => {
    try {
      const isValid = await categorySchema.isValid(data);
      if (isValid) {
        if (selected) {
          await request.put(`category/${selected}`, data);
        } else {
          // await axios.post(
          //   "https://640b314281d8a32198dce736.mockapi.io/api/v1/category",
          //   data
          // );
          await request.post("category", data);
        }
        reset();
        getData();
        closeModal();
      }
    } catch (err) {
      console.log(err);
    }
  };

  const edit = async (id) => {
    let { data } = await request.get(`category/${id}`);
    reset(data);
    openModal();
    setSelected(id);
  };

  const deleteCategory = async (id) => {
    let check = confirm("Are you sure you want to delete this category?");
    if (check) {
      await request.delete(`category/${id}`);
      getData();
    }
  };
  console.log("Render");
  return (
    <div className="container">
      <div className="input-group my-3">
        <input
          onChange={(e) => setSearch(e.target.value)}
          value={search}
          type="text"
          className="form-control"
          placeholder="Search"
          aria-label="Search"
        />
        <button className="input-group-text" onClick={openModal}>
          Add
        </button>
      </div>
      <table className="table">
        <thead>
          <tr>
            <th scope="col">No</th>
            <th scope="col">Name</th>
            <th scope="col">Image</th>
            <th className="text-end" scope="col">
              Action
            </th>
          </tr>
        </thead>
        <tbody>
          {categories.map(({ name, image, id }, index) => (
            <tr key={id}>
              <th>{index + 1}</th>
              <td>{name}</td>
              <td>
                <LazyLoadImage src={image} height={50} />
              </td>
              <td className="text-end">
                <button className="btn btn-primary" onClick={() => edit(id)}>
                  Edit
                </button>
                <button
                  className="btn btn-danger"
                  onClick={() => deleteCategory(id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Modal ariaHideApp={false} isOpen={isOpen} onRequestClose={closeModal}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <div className="input-group mb-3">
            <input
              {...register("name")}
              type="text"
              className="form-control"
              placeholder="Name"
            />
          </div>
          {errors.name && (
            <p role="alert" className="text-danger">
              {errors.name.message}
            </p>
          )}
          <div className="input-group mb-3">
            <input
              {...register("image")}
              type="text"
              className="form-control"
              placeholder="Image"
            />
          </div>
          {errors.image && (
            <p role="alert" className="text-danger">
              {errors.image.message}
            </p>
          )}
          <button className="btn btn-danger me-3" onClick={closeModal}>
            Close
          </button>
          <button type="submit" className="btn btn-success">
            {selected ? "Save" : "Add"}
          </button>
        </form>
      </Modal>
    </div>
  );
};

export default CategoriesP;
